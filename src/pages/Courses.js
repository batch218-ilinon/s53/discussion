import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'

export default function Courses(){

	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} course = {course}/>
		)
	})

	return (
		<>
		{courses}
		</>
	)
}